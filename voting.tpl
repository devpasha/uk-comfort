{if !empty($s_userLogin)}
<style>
    .numb-submit input[type="submit"] {
        display: block;
    }
</style>
{/if}
<div class="content">
    <div class="title-vote">
        <div class="fixed">
            <h1>Голосування</h1>
            {if !empty($s_userLogin)}
                <div id="create-vote" data-toggle="modal" data-target="#myModalVote">
	                <img src="/data/html/img/create-vote.png" alt="">
	                <h2>Створити голосування</h2>
            	</div>
            {/if}
        </div>
    </div>
             <!-- Modal -->
                <div id="myModalVote" class="modal fade" role="dialog">
                    <div class="modal-dialog-vote">
                        <!-- Modal content-->
                        <div class="modal-content-vote">
                            <div id="createVoteModalContent">
                                <h1>Створити голосування</h1>
                                <form action="/ua/voting/" method="post" id="createVoteForm">
                                    <h2>Введіть Ваше питання</h2>
                                    <input type="text" name="vote_title" required="required"><br>
                                    <h2>Задайте варіанти відповідей</h2>
                                    <div class="variants">
                                        <input type="text" placeholder="Варіант 1" name="option0" required="required"><br>
                                        <input type="text" placeholder="Варіант 2" name="option1" required="required"><br>
                                    </div>
                                    <div id="addvar"><h5 onclick="appendVar()">+</h5><h6>Додати варіант відповіді</h6></div>
                                    <input type="submit" value="Створити голосування">
                                </form>
                            </div>
                            <div id="createVoteModalContent1">
                                <h1>Голосування створено. Воно буде опубліковане після перевірки модераторами.</h1>
                                <button data-toggle="modal" data-target="#myModalVote" id="voteRes">Ок</button>
                            </div>
                        </div>
                    </div>
                </div>
    <div class="votings">
        {foreach from=$s_allVotes item=vote}
{*счетчик количества вариантов в опросе*}
            {assign var="i" value=0}
            <div class="fixed">
                 <div class="voting">
                    <div class="voting-left">
                    <h2>Голосування</h2>
                    <h1>{$vote.vote_title}</h1>
                    <br>
                        <form action="/ua/voting/" method="post">
                        <input type="hidden" name="param" value="{$vote.vote_title}">{* что бы знать к какому опросу обращаемся*}
                        {foreach from=$s_allVotesOptions item=voteOption}
                          {if $vote.vote_title == $voteOption.vote_title}
{*сравниваем  названия опроса и заголовок для каждого из вариантов. Пока находимя в том же опросе - выводим названия вариантов.*}
                            <div class="input">
                                <label class="switch">
                                <input type="radio" name="voting" value="{$voteOption.vote_option}">
                                <span class="slider round"></span>
                                </label>
                                <h6><span class="vote-grey"></span>{$voteOption.vote_option}</h6>
                                <br>
                                {assign var="i" value=$i+1}
                            </div>
                          {/if}
                        {/foreach}
                        <div class="stat-data" style="display: none">
                            {while $i>0}
                                {foreach from=$s_allVotesOptions item=voteOption}
                                    {if $vote.vote_title == $voteOption.vote_title}
                                        <p>Всього голосів: <span class='total'>{$vote.total_votes}</span></p>
                                        <p ><span class="variantValue">{$voteOption.quantity}</span>: <span class="voted">{$voteOption.quantity}</span></p>
                                        {$i--}
                                    {/if}
                                {/foreach}
                            {/while}
                        </div>
                            {if !empty($s_userLogin)}
                                    {if $vote.voted >= 1}
                                        <div class="disabled-vote" id="state_vote">Ви вже проголосували</div>
                                    {else}
                                        <input type="submit" value="Голосувати" class="vote-submit">
                                        <div style="display: none" id="state_vote">Голосувати</div>
                                    {/if}
                            {else}
                                 <div class="unsign-vote">Голосувати можуть лише зареєстровані користувачі</div>
                                 <div style="display: none" id="state_vote">Голосувати</div>
                            {/if}
                                <div class="numb-submit">
                                    <div class="numb"><h6><span class="vote-grey">Всього проголосувало:</span><span class="vote-grey" id="total_votes_display">{$vote.total_votes}</span></h6></div>
                                </div>
                        </form>
                    </div>
                        <div class="voting-right">
                            <h2>Статистика голосування:</h2>
                                <div class="stat-field">
                                    <div class="bgrd-stat">
                                        <div class="line"><div class="linee"></div><div class="percent">10%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">20%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">30%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">40%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">50%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">60%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">70%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">80%</div></div>
                                        <div class="line"><div class="linee"></div><div class="percent">90%</div></div>
                                        <div class="line"><div class="line9"></div><div class="percent">100%</div></div>
                                    </div>
                                        <div class="stat-content"></div>
                                </div>
                        </div>
                 </div>
            </div>
        {/foreach}
    </div>
    <div id="page" style="display: none">votings</div>
</div>
