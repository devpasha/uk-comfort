<?php
// Выбираем все названия опросов, в которых поголосовал зарегистрированный пользователь
$alreadyVoted = $sql->getAll("SELECT vote_title FROM vote_answers WHERE login ='".$_SESSION['userLogin']."'");
$smarty->assign('s_alreadyVoted', $alreadyVoted);

//Выводим список всех опросов
$allVotes = $sql->getAll("SELECT * FROM votes WHERE `published`= 'yes' ORDER BY `id` DESC");

// элемент voted хранит колличество ответов от конкретного пользователя в конкретном опросе. Если он не будет равен 0, будет показано сообщение: Вы уже голосовали в этом опросе
foreach ($allVotes as $vote) {

    $votes_array[] = array(
        'vote_title' => $vote['vote_title'],
        'id' => $vote['id'],
        'published' => $vote['published'],
        'total_votes' => $vote['total_votes'],
        'voted' => $sql -> getCount("SELECT * FROM vote_answers WHERE login ='".$_SESSION['userLogin']."' and `vote_title`='".$vote['vote_title']."'")
    );
}

$smarty -> assign('s_allVotes', $votes_array);

// все существующие варианты ответов в опросах
$allVotesOptions = $sql->getAll("SELECT * FROM vote_cases  ORDER BY `vote_title` DESC");
$smarty -> assign('s_allVotesOptions', $allVotesOptions);

// Если произошло голосвание в каком-то опросе
if(!empty($_POST["param"]) && !empty($_POST["voting"])){
// считаем, сколько было голосов суммарно в этом опросе
    $curTotalVotes = $sql->getAll("SELECT total_votes FROM votes WHERE `vote_title`= '".$_POST['param']."'"); // сколько сейчас всего голосов в этом опросе
    $updTotalVotes = $curTotalVotes[0]['total_votes']+1;
    $sql->exec("update votes set total_votes = '".$updTotalVotes."' WHERE `vote_title`= '".$_POST['param']."'"); // заносим новое значение

    // считаем, сколько голосов отдано за конкретный вариант в конкретном опросе
    $curQuantity = $sql->getAll("SELECT quantity FROM vote_cases WHERE `vote_title`= '".$_POST['param']."' AND `vote_option`='".$_POST['voting']."'");
    $updTotalVotes = $curQuantity[0]['quantity']+1;
    $sql->exec("update vote_cases set quantity = '".$updTotalVotes."' WHERE `vote_title`= '".$_POST['param']."' AND `vote_option`='".$_POST['voting']."'");

    // заносим в базу кто за какой вариант в каком опросе проголосовал
    $sql->exec("INSERT INTO vote_answers(vote_title, vote_option, login) VALUES ('".$_POST["param"]."', '".$_POST['voting']."', '".$_SESSION['userLogin']."')");

    unset($_POST["param"]);
    unset($_POST["voting"]);
    header('Location: /ua/voting/');
   }

   // -------------------------------- Создание нового опроса ------------------------------------------
if (isset($_POST['vote_title'])){
    $arrCases=$_POST;
    unset($arrCases['vote_title']);// убираем элемент массива, все что осталось - это созданные варианты при голосовании
    $counter=count($arrCases);
    for($i=0; $i<$counter; $i++){
        $sql->exec("INSERT INTO vote_cases(vote_title, vote_option, quantity) VALUES ('".$_POST["vote_title"]."','".$_POST['option'.$i]."',0)");
        unset($_POST['option'.$i]);
    }
    $voteCreator = $sql->getAll("SELECT account, pib  FROM `inhabitant` WHERE login='".$_SESSION['userLogin']."' ");
    $crAcc= $voteCreator[0]['account'];
    $crPib = $voteCreator[0]['pib'];
    $sql->exec("INSERT INTO votes(vote_title, published, total_votes, owner) VALUES('".$_POST["vote_title"]."', 'no', 0, '".$crPib."' )");
    mail(" uk.akvareli@gmail.com", "Створення нового опитування", "Мешканець $crPib (особовий рахунок: $crAcc) створив нове опитування, яка потребує модерації! ");

unset($_POST['vote_title']);
header('Location: /ua/voting/');
}
    // -------------------------------- /Создание нового опроса ------------------------------------------


$smarty -> assign('id',$id);
$smarty->assign('s_userLogin', $_SESSION['userLogin']);
$smarty -> assign('selected', basename(__FILE__, '.php'));
$smarty -> display("site/body/header.tpl");
$smarty -> display("site/pages/".basename(__FILE__, '.php').".tpl");
$smarty -> display("site/body/footer.tpl");
